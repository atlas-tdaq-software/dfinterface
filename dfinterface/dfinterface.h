#ifndef DFINTERFACE_DFINTERFACE_H
#define DFINTERFACE_DFINTERFACE_H

#include <chrono>
#include <memory>
#include <string>
#include <vector>

#include <boost/property_tree/ptree_fwd.hpp>

#include "eformat/StreamTag.h"
#include "eformat/ROBFragmentNoTemplates.h"
#include "eformat/write/eformat.h"
#include "ers/ers.h"
#include "hltinterface/DCM_ROBInfo.h"

namespace daq
{

/*! \brief Interface between HLTPU and data source
 *
 *  This abstract interface represents a service (such as the DCM) that provides event data to be
 *  analysed by the High-Level Trigger and is informed of the final trigger decision. It is meant
 *  to be implemented in a shared library.
 *
 *  A communication session with the service is created using the createSession() function. The
 *  returned \ref Session pointer is used to obtain new events to process and to inform the service
 *  of the trigger decision.
 *
 *  \remark The implementations of this interface must provide the following (strong) thread safety
 *  guarantees:
 *  \li Concurrent calls to free functions are safe.
 *  \li Concurrent access to different instances of a class is safe.
 *  \li Concurrent access to the same instance of a class is safe.
 *
 *  \remark The implementation must be able to deal with the effects of a fork() system call without
 *  issues, provided that the following conditions hold:
 *  \li For each instance \c s of \ref Session,
 *      \code
 *      s.isOpen() == false;
 *      \endcode
 *  \li There are no \ref Event instances
 *
 *  \remark The exceptions explicitly specified by this interface are only those that can be
 *  meaningfully handled by the caller. It should not be assumed that functions with no documented
 *  exceptions will not throw, however implementations should only throw undocumented exceptions in
 *  case of unrecoverable errors.
 */
namespace dfinterface
{

//! \cond DOXYGEN_IGNORE

class Session;
class Event;

//! \endcond

/*! \brief Creates a new dfinterface \ref Session with the given configuration.
 *
 *  \param[in] configuration Configuration tree for the Session. The content of the tree
 *      is specific to the dfinterface implementation used.
 *
 *  \exception BadConfiguration is thrown if the configuration tree does not contain valid data.
 */
extern "C" std::unique_ptr<Session> createSession(const boost::property_tree::ptree& configuration);

/*! \brief Represents a dfinterface communication Session.
 *
 *  It enables the interaction with a service that provides event data to be analysed (via the
 *  getNext(), tryGetNextUntil() or tryGetNextFor() methods) and is informed of the final
 *  trigger decision (via the accept() or reject() methods).
 */
class Session
{

public:

  /*! \brief Opens the Session.
   *
   *  \param[in] clientName Name of the application opening the Session. The application name
   *      must be suitable to report a misbehaving application to the Run Control infrastructure.
   *
   *  \exception CommunicationError is thrown if the Session can not be created due to
   *      communication issues with the service, e.g. the service is unreachable or does not respond
   */
  virtual void open(const std::string& clientName) = 0;

  /*! \brief Determines whether the Session is open.
   */
  virtual bool isOpen() const = 0;

  /*! \brief Closes the Session.
   */
  virtual void close() = 0;

  /*! \brief Returns a pointer to the next \ref Event object to be processed.
   *
   *  Blocks indefinitely while waiting for new events.
   *
   *  \exception NoMoreEvents is thrown if there are no more events to be processed.
   *  \exception CommunicationError is thrown in case of network communication issues
   */
  virtual std::unique_ptr<Event> getNext() = 0;

  /*! \brief Returns a pointer to the next \ref Event object to be processed.
   *
   *  Blocks until an event can be obtained, or the specified time is reached.
   *
   *  \exception OperationTimedOut is thrown if an event could not be obtained before \c absTime.
   *  \exception NoMoreEvents is thrown if there are no more events to be processed.
   *  \exception CommunicationError is thrown in case of network communication issues
   */
  virtual std::unique_ptr<Event> tryGetNextUntil(
      const std::chrono::steady_clock::time_point& absTime) = 0;

  /*! \brief Returns a pointer to the next \ref Event object to be processed.
   *
   *  Equivalent to:
   *  \code
   *  tryGetNextUntil(std::chrono::steady_clock::now() + relTime)
   *  \endcode
   */
  virtual std::unique_ptr<Event> tryGetNextFor(
      const std::chrono::steady_clock::duration& relTime) = 0;

  /*! \brief Marks the event as accepted by the High-Level Trigger.
   *
   *  \param[in] event An \ref Event object obtained from getNext(), tryGetNextUntil() or
   *      tryGetNextFor()
   *  \param[in] hltr Points to serialized read::FullEventFragment, it should contain High Level
   *      trigger information, streamTags, pscErrors and HLT fragments
   *
   *  \exception CommunicationError is thrown in case of network communication issues
   *
   */
  virtual void accept(std::unique_ptr<Event> event,
      std::unique_ptr<uint32_t[]> hltr) = 0;

  /*! \brief Marks the event as rejected by the High-Level Trigger.
   *
   *  \param[in] event An \ref Event object obtained from getNextEvent(),
   *      tryGetNextEventUntil() or  tryGetNextEventFor()
   *
   *  \exception CommunicationError is thrown in case of network communication issues
   */
  virtual void reject(std::unique_ptr<Event> event) = 0;

  virtual ~Session()
  {
  }

};

/*! \brief Provides access to the event data.
 *
 *  \remark \ref Event pointers are obtained from a \ref Session instance and they must be returned
 *  to that instance when the processing is complete.
 */
class Event
{

public:

  /*! \brief Returns the Level-1 ID of the event.
   */
  virtual uint32_t l1Id() = 0;

  /*! \brief Returns the global ID of the event.
   */
  virtual uint64_t gid() = 0;

  /*! \brief Returns the luminosity block of the event.
   */
  virtual uint16_t lumiBlock() = 0;

  /*! \brief Request l1results of the next event
   *
   * A FullEventFragment is filled with partial event data containing at the
   * minimum the L1 RoI ROBs and a correctly filled event header. The fragment
   * is serialised and the ownership is passed to \p l1Result. The caller is
   * responsible for releasing the pointer after use.
   *
   * The following fields of the header should be correctly set to the
   * respective values for a given event:
   * version, global_id, run_type, run_no, lumi_block, bc_id, bc_time_seconds,
   * bc_time_nanoseconds, lvl1_id, lvl1_trigger_type, lvl1_trigger_info
   *
   * In addition:
   * source_id should be FULL_SD_EVENT
   * status should be normally 0
   * compression_type should be UNCOMPRESSED
   * checksum can be NO_CHECKSUM
   *
   *  \param[out] l1Result reference to a unique_ptr which should take ownership of a
   *      serialized FullEventFragment containing LVL1 result ROBs and event header
   */
  virtual void getL1Result(std::unique_ptr<uint32_t[]>& l1Result) = 0;

  /*! \brief Informs the dfinterface implementation that the specified ROBs are likely to be requested in
   *      the future.
   *
   *  \param[in] robIds IDs of the requested ROBs.
   *
   *  \exception CommunicationError is thrown in case of network communication issues
   */
  virtual void mayGetRobs(const std::vector<uint32_t>& robIds) = 0;

  /*! \brief Retrieves the specified ROB fragments.
   *
   *  \param[in] robIds IDs of the requested ROBs.
   *  \param[out] robInfos The requested ROBFragments object are pushed back into this vector
   *              together with DC time statistics according to the structure hltinterface::DCM_ROBInfo
   *
   *  \exception CommunicationError is thrown in case of network communication issues
   *
   *  The ROBFragment objects in \c robs are valid for the entire lifetime of this \ref Event
   *  object. If a fragment cannot be retrieved due to errors, a special empty fragment will be
   *  provided instead.
   */
  virtual void getRobs(const std::vector<uint32_t>& robIds,
      std::vector<hltinterface::DCM_ROBInfo>& robInfos) = 0;

  /*! \brief Retrieves all the available ROB fragments.
   *
   *  \param[out] robInfos The requested ROBFragments object are pushed back into this vector
   *              together with DC time statistics according to the structure hltinterface::DCM_ROBInfo
   *
   *  \exception CommunicationError is thrown in case of network communication issues
   *
   *  The ROBFragment objects in \c robs are valid for the entire lifetime of this \ref Event
   *  object. If a fragment cannot be retrieved due to errors, a special empty fragment will be
   *  provided instead. Fragments already retrieved with get_robs() are nevertheless included in
   *  \c robs.
   */
  virtual void getAllRobs(std::vector<hltinterface::DCM_ROBInfo>& robInfos) = 0;

  virtual ~Event()
  {
  }

};

} // namespace dfinterface

/// Base abstract issue
ERS_DECLARE_ISSUE(dfinterface, Issue, ERS_EMPTY, ERS_EMPTY)

ERS_DECLARE_ISSUE_BASE(dfinterface, NoMoreEvents, dfinterface::Issue,
    "No more events available" << reason, ERS_EMPTY, ((std::string) reason))

ERS_DECLARE_ISSUE_BASE(dfinterface, BadConfiguration, dfinterface::Issue,
    "Invalid configuration" << reason, ERS_EMPTY, ((std::string) reason))

ERS_DECLARE_ISSUE_BASE(dfinterface, OperationTimedOut, dfinterface::Issue,
    "Timeout expited" << reason, ERS_EMPTY, ((std::string) reason))

ERS_DECLARE_ISSUE_BASE(dfinterface, CommunicationError, dfinterface::Issue,
    "Communication error" << reason, ERS_EMPTY, ((std::string) reason))

} // namespace daq

#endif // !defined(DFINTERFACE_DFINTERFACE_H)
