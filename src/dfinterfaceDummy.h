#ifndef DF_INTERFACE_DUMMY
#define DF_INTERFACE_DUMMY

#include "dfinterface/dfinterface.h"
#include "eformat/eformat.h"
#include "ers/ers.h"
#include <set>

namespace daq
{

namespace dfinterface
{

/*! \brief Dummy implementation of the dfinterface Session
 */
class SessionDummy: public Session
{
public:

  /*! \brief C'tor
   *
   *  \param[in] nEvts Max number of event to process (used to simulate end of run)
   */
  SessionDummy(uint32_t nEvts);

  /*! \brief Opens the session.
   *
   *  \param[in] client_name Name of the application opening the session. The application name
   *      must be suitable to report a misbehaving application to the Run Control infrastructure.
   *
   *  \exception CommunicationError is thrown if the session can not be created due to
   *      communication issues with the service (e.g. the service is unreachable or does not
   *      respond).
   */
  virtual void open(const std::string& client_name);

  /*! \brief Determines whether the session is open.
   */
  virtual bool isOpen() const
  {
    return m_open;
  }

  /*! \brief Closes the session.
   */
  virtual void close();

  /*! \brief Returns a pointer to the next \ref event object to be processed.
   *
   *  Blocks indefinitely while waiting for new events.
   *
   *  \exception noMoreEvents is thrown if there are no more events to be processed.
   */
  virtual std::unique_ptr<Event> getNext();

  /*! \brief Returns a pointer to the next \ref event object to be processed.
   *
   *  Blocks until an event can be obtained, or the specified time is reached.
   *
   *  \exception OperationTimedOut is thrown if an event could not be obtained before \c absTime.
   *  \exception NoMoreEvents is thrown if there are no more events to be processed.
   */
  virtual std::unique_ptr<Event> tryGetNextUntil(
      const std::chrono::steady_clock::time_point& absTime);

  /*! \brief Returns a pointer to the next \ref event object to be processed.
   *
   *  Equivalent to:
   *  \code{.cpp}
   *  tryGetNextUntil(std::chrono::system_clock::now() + relTime)
   *  \endcode
   */
  virtual std::unique_ptr<Event> tryGetNextFor(
      const std::chrono::steady_clock::duration& relTime);

  /*! \brief Marks the event as accepted by the High-Level Trigger.
   *
   *  \param[in] event An \ref event object obtained from getNext(), tryGetNextUntil() or
   *      tryGetNextFor()
   *  \param[in] triggerInfo Vector of High-Level Trigger information 32-bit words to be stored in
   *      the event header.
   *  \param[in] streamTags Vector of stream tags to be stored in the event header. The stream tags
   *      can contain partial event building lists.
   *  \param[in] pscErrors Vector of 32-bit PSC error words to be stored as additional status words
   *      in the event header.
   *  \param[in] hltFragment A ROBFragment containing High-Level Trigger information to be appended
   *      to the event.
   *
   *  \remark This method performs a deep copy of the \c hltFragment. The memory referenced by it
   *  can be safely freed after the method call.
   */
  virtual void accept(std::unique_ptr<Event> event,
      const std::vector<uint32_t>& triggerInfo,
      const std::vector<eformat::helper::StreamTag>& streamTags,
      const std::vector<uint32_t>& pscErrors,
      const std::vector<eformat::read::ROBFragment>& hltFragments);

  /*! \brief Marks the event as rejected by the High-Level Trigger.
   *
   *  \param[in] event An \ref event object obtained from getNextEvent(),
   *      tryGetNextEventUntil() or  tryGetNextEventFor()
   */
  virtual void reject(std::unique_ptr<Event> event);

  virtual ~SessionDummy();

private:
  bool m_open; ///< Keep track of the session status
  std::string m_clientName; ///< Keep track of the client name (for error messages)
  uint32_t m_maxNbrEvts; ///< Max number of events before throwing no_more_events
  uint32_t m_eventCnt; ///< Count the number of events

};

/*! \brief Dummy implementation of the Event interface
 *
 *   NB: In the data-taking implementations the dfinterface implementation forwards the 
 *   requests to the DCM via a dedicated socket
 *
 */
class EventDummy: public Event
{
public:

  /// C'tor
  EventDummy();

  /*! \brief Returns the Level-1 ID of the event.
   */
  virtual uint32_t l1Id()
  {
    return m_l1Id;
  }

  /*! \brief Returns the flobal ID of the event.
   */
  virtual uint64_t gid()
  {
    return m_gid;
  }

  /*! \brief Returns the luminosity block of the event.
   */
  virtual uint16_t lumiBlock()
  {
    return m_lumiBlock;
  }


  /*! \brief Stores the Level-1 Result fragments for this event into the provided vector.
   *
   *  \param[out] l1result The L1Result ROBFragment objects are pushed back into this vector.
   *
   *  - In this dummy implementation
   *     1. generate dummy fragments (eformat::write::ROBFragment)
   *     2. serialize them in memory
   *     3. construct eformat::read::ROBFragment objects from the serialized data
   *     4. fill the vector<eformat::read::ROBFragment>
   *  - In the DCM implementation forward the list to the DCM via the socket
   */
  virtual void getL1Result(std::vector<eformat::read::ROBFragment>& l1result);

  /*! \brief Informs the dfinterface implementation that the specified ROBs are likely to be requested in
   *      the future.
   *
   *  \param[in] robIds IDs of the requested ROBs.
   *
   *  - In this dummy implementation the vector is ignored.
   *  - In the DCM implementation the vector is supposed to be forwarded to the DCM via a socket
   */
  virtual void mayGetRobs(const std::vector<uint32_t>& robIds);

  /*! \brief Retrieves the specified ROB fragments.
   *
   *  \param[in] robIds IDs of the requested ROBs.
   *  \param[out] robInfos The requested ROBFragments object are pushed back into this vector
   *              together with DC time statistics according to the structure hltinterface::DCM_ROBInfo
   *
   *  The ROBFragment objects in \c robs are valid for the entire lifetime of this \ref event
   *  object. If a fragment cannot be retrieved due to errors, a special empty fragment will be
   *  provided instead.
   *
   *  In this dummy implementation:
   *     1. generate dummy fragments (eformat::write::ROBFragment) with the specified ROBIds
   *     2. serialize them in memory
   *     3. construct eformat::read::ROBFragment objects from the serialized data
   *     4. construct hltinterface::DCM_ROBInfo (with dummy DC times)
   *     5. fill the vector<hltinterface::DCM_ROBInfo>
   */
  virtual void getRobs(const std::vector<uint32_t>& robIds,
      std::vector<hltinterface::DCM_ROBInfo>& robInfos);

  /*! \brief Retrieves all the available ROB fragments.
   *
   *  \param[out] robInfos The requested ROBFragments object are pushed back into this vector
   *              together with DC time statistics according to the structure hltinterface::DCM_ROBInfo
   *
   *  The ROBFragment objects in \c robs are valid for the entire lifetime of this \ref event
   *  object. If a fragment cannot be retrieved due to errors, a special empty fragment will be
   *  provided instead. Fragments already retrieved with getRobs() are nevertheless included in
   *  \c robs.
   *
   *  In this dummy implementation:
   *     1. generate all dummy fragments (eformat::write::ROBFragment)
   *        - Assume a single detector ID
   *     2. serialize them in memory
   *     3. construct eformat::read::ROBFragment objects from the serialized data
   *     4. construct hltinterface::DCM_ROBInfo (with dummy DC times)
   *     5. fill the vector<hltinterface::DCM_ROBInfo>
   */
  virtual void getAllRobs(std::vector<hltinterface::DCM_ROBInfo>& robInfos);

private:
  ///
  /// Generate a dummy L1Result (a CTP ROB) and serialize it in m_buffer at m_offset=0
  ///
  void generateRoI();

  ///
  /// Generate dummy ROBs, serialize them in m_buffer and returns the pointers to them
  ///
  void generateRobs(const std::vector<uint32_t>& robIds, std::vector<uint32_t*> &fragPtrs);

  /// D'tor
  virtual ~EventDummy();

private:

  uint32_t m_l1Id;      ///< Keep track of the L1Id of the event
  uint64_t m_gid;       ///< Keep track of the GId of the event
  uint16_t m_lumiBlock; ///< Keep track of the luminosity block of the event
  uint32_t* m_buffer;   ///< Buffer to store all the ROBs (like the Shared Heap Array)
  uint32_t m_offset;    ///< Pointer to the begin of the free area
  std::set<uint32_t> m_robs; ///< Keep track of the ROBs already fetched
  std::set<uint32_t> m_mayGet; ///< Keep track of the ROBs that can be requested later
                               ///< NB: In the DCM implementation the DCM itself keep track of it
};

} // namespace dfinterface
} // namespace daq

/// ------- Utilities ----------------------------------------------

/*! \brief Function emulating a socket
 */
void send2socket(const uint32_t *data, uint32_t size);

#endif
