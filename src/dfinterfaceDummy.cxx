#include "dfinterfaceDummy.h"
#include "eformat/write/ROBFragment.h"
#include "ers/ers.h"
#include <stdio.h>
#include <set>
#include <boost/property_tree/ptree.hpp>
#include <chrono>

#define BUFSIZE 4000000

/// Global object to emulate the service in charge of providing the available ROBs
extern std::set<uint32_t> robsInTheRun;

namespace daq
{

namespace dfinterface
{

// DLL entry point
std::unique_ptr<Session> createSession(const boost::property_tree::ptree& configuration)
{
  // Exception throwing example
  if (0)
    throw BadConfiguration(ERS_HERE, "One or more components failed the transition.");

  uint32_t nEvts = configuration.get("MaxNumEvents", 10);
  std::unique_ptr<Session> s(new SessionDummy(nEvts));
  ERS_LOG("Created new SessionDummy ");
  return std::move(s);
}

SessionDummy::SessionDummy(uint32_t nEvts) :
    m_open(false), m_clientName(""), m_maxNbrEvts(nEvts), m_eventCnt(0)
{
}

void SessionDummy::open(const std::string& client_name)
{
  m_open = true;
  m_clientName = client_name;
  ERS_LOG("Opened connection with the DCM socket for " << m_clientName);
}

void SessionDummy::close()
{
  m_open = false;
  ERS_LOG("Closed connection for " << m_clientName);
}

std::unique_ptr<Event> SessionDummy::getNext()
{
  // Emulate end of run
  if (++m_eventCnt > m_maxNbrEvts)
    throw NoMoreEvents(ERS_HERE, "End of run");

  // Return a dummy event
  std::unique_ptr<Event> p( new EventDummy );
  return std::move(p);
}

std::unique_ptr<Event> SessionDummy::tryGetNextUntil(
    const std::chrono::steady_clock::time_point& absTime)
{
  // Emulate end of run
  if (++m_eventCnt > m_maxNbrEvts)
    throw NoMoreEvents(ERS_HERE, "End of run");

  // Emulate the operations needed to get the data
  while(0)
  { ERS_LOG("Wait for an event and keep checking absTime until: " << absTime.time_since_epoch().count());}

  // Return a dummy event
  std::unique_ptr<Event> p(new EventDummy);
  return std::move(p);
}

std::unique_ptr<Event> SessionDummy::tryGetNextFor(
    const std::chrono::steady_clock::duration& relTime)
{
  // Emulate end of run
  if (++m_eventCnt > m_maxNbrEvts)
    throw NoMoreEvents(ERS_HERE, "End of run");

  // Emulate the operations needed to get the data
  while(0)
  { ERS_LOG("Wait for an event for " << relTime.count() );}

  // Return a dummy event
  std::unique_ptr<Event> p(new EventDummy);
  return std::move(p);
}

void SessionDummy::accept(std::unique_ptr<Event> event,
    const std::vector<uint32_t>& triggerInfo,
    const std::vector<eformat::helper::StreamTag>& streamTags,
    const std::vector<uint32_t>& pscErrors,
    const std::vector<eformat::read::ROBFragment>& hltFragments)
{
  ERS_LOG("Event " << event->l1Id() << " accepted");
  // Serialize triggerInfo
  ERS_LOG("Serialize vector<uint32_t>& triggerInfo to socket: " << triggerInfo.size() << " words");
  send2socket(triggerInfo.data(), triggerInfo.size());

  // Serialize pscErrors
  ERS_LOG("Serialize vector<uint32_t>& psc_errors to socket: " << pscErrors.size() << " words");
  send2socket(pscErrors.data(), pscErrors.size());

  // Serialize stream tags
  uint32_t tagSize = eformat::helper::size_word(streamTags);
  ERS_LOG("Serialize vector<StreamTag> streamTags to socket: " << tagSize << " words");
  uint32_t *tagWords = new uint32_t[tagSize];
  eformat::helper::encode(streamTags, tagSize, tagWords);
  send2socket(tagWords, tagSize);
  delete[] tagWords;

  // Serialize HLTROBs
  for (auto& hltFragment : hltFragments) {
    ERS_LOG("Serialize HLTROB to socket: " << hltFragment.fragment_size_word() << " words");
    const uint32_t *p = hltFragment.start(); // FIXME: is it safe?
    send2socket(p, hltFragment.fragment_size_word());
  }
}

void SessionDummy::reject(std::unique_ptr<Event> event)
{
  ERS_LOG("Event " << event->l1Id() << " rejected");
  send2socket(NULL, 0);

  if (0)
    throw CommunicationError(ERS_HERE, "Reset connection?");
}

SessionDummy::~SessionDummy()
{
  ERS_LOG("Dummy session destroyed");
}

EventDummy::EventDummy()
{
  ERS_LOG("...");
  m_buffer = new uint32_t[BUFSIZE];
  m_offset = 0;
  generateRoI();
}

EventDummy::~EventDummy()
{
  delete[] m_buffer;
}

void EventDummy::getL1Result(std::vector<eformat::read::ROBFragment>& l1result)
{
  // For simplicity, let's assume a R1Result with a single ROB
  eformat::read::ROBFragment ctpRob(m_buffer);
  m_robs.insert(ctpRob.source_id());
  l1result.push_back(ctpRob);
}

void EventDummy::mayGetRobs(const std::vector<uint32_t>& robIds)
{
  for (auto &i : robIds) {
    m_mayGet.insert(i);
  }
  send2socket(robIds.data(), robIds.size());
}

void EventDummy::getRobs(const std::vector<uint32_t>& robIds,
    std::vector<hltinterface::DCM_ROBInfo>& robInfos)
{
  // Simulate the data collection generating dummy data
  // For the sake of simplicity assign the same DC time to all the ROBs
  auto start = std::chrono::steady_clock::now(); // DC time statistics
  std::vector<uint32_t*> ptrs;
  generateRobs(robIds, ptrs);
  auto stop = std::chrono::steady_clock::now(); // DC time statistics

  // With the rob data pointers fill the vector<ROBFragment> and update the set of fetched robs
  for (auto &i : ptrs) {
    eformat::read::ROBFragment rob(i);
    robInfos.emplace_back(hltinterface::DCM_ROBInfo(rob, false, start, stop));
    m_robs.insert(rob.source_id());
  }
}

// Emulate the collection of all the missing ROBs
void EventDummy::getAllRobs(std::vector<hltinterface::DCM_ROBInfo>& robInfos)
{
  // Calculate the list of missing ROBs
  std::vector<uint32_t> diff;
  std::set_difference(robsInTheRun.begin(), robsInTheRun.end(),
      m_robs.begin(), m_robs.end(),
      std::inserter(diff, diff.begin()));

  // "Fetch" the missing ROBs
  // For the sake of simplicity assign the same DC time to all the ROBs
  auto start = std::chrono::steady_clock::now(); // DC time statistics
  std::vector<uint32_t*> ptrs;
  generateRobs(diff, ptrs);
  auto stop = std::chrono::steady_clock::now(); // DC time statistics

  // With the rob data pointers fill the vector<ROBFragment> and update the set of fetched robs
  for (auto &i : ptrs) {
    eformat::read::ROBFragment rob(i);
    robInfos.emplace_back(hltinterface::DCM_ROBInfo(rob, false, start, stop));
    m_robs.insert(rob.source_id());
  }

}

void EventDummy::generateRoI()
{
  // Keep track of the L1ID
  static uint32_t l1Id = 0;
  l1Id++;
  m_l1Id = l1Id;
  m_gid  = l1Id;    // FIXME 
  m_lumiBlock = 0;  // FIXME 

  // Dummy CTP fragment (just two words payload)
  uint32_t source_id = eformat::helper::SourceIdentifier(eformat::TDAQ_CTP, 1).code();
  const uint32_t run_no = 1;
  const uint32_t bc_id = 1;
  const uint32_t lvl1_type = 1;
  const uint32_t detev_type = 1;
  const uint32_t statPos = 1;
  const uint32_t data[2] = { 0xDF000001, 0xDF000002 };
  const uint32_t ndata = sizeof(data) / sizeof(uint32_t);
  eformat::write::ROBFragment ctp_rob(source_id, source_id, run_no, l1Id, bc_id, lvl1_type, detev_type, ndata, data, statPos);
  const eformat::write::node_t * nodes = ctp_rob.bind();
  eformat::write::copy(*nodes, m_buffer, ctp_rob.size_word());
  m_offset = ctp_rob.size_word();
}

void EventDummy::generateRobs(const std::vector<uint32_t>& robIds, std::vector<uint32_t*> &fragPtrs)
{
  eformat::read::ROBFragment ctpRob(m_buffer);

  const uint32_t data[2] = { 0xDF000001, 0xDF000002 };
  const uint32_t ndata = sizeof(data) / sizeof(uint32_t);

  for (uint32_t i = 0; i < robIds.size(); i++) {
    uint32_t source_id = robIds[i];
    eformat::write::ROBFragment rob(source_id, source_id, ctpRob.rod_run_no(), ctpRob.rod_lvl1_id(),
        ctpRob.rod_bc_id(), ctpRob.rod_lvl1_trigger_type(), ctpRob.rod_detev_type(), ndata, data, 1);
    const eformat::write::node_t * nodes = rob.bind();
    eformat::write::copy(*nodes, m_buffer + m_offset, rob.size_word());
    fragPtrs.push_back(m_buffer + m_offset);
    m_offset += rob.size_word();
  }
}

} // namespace dfinterface
} // namespace daq

// Emulate a socket
void send2socket(const uint32_t *data, uint32_t size)
{
  //ERS_LOG("Serializing " << size << " words");
  data++;
  size++;
}

