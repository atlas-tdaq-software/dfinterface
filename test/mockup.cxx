#include <iostream>
#include "ers/ers.h"
#include "dfinterface/dfinterface.h"
#include "dfinterface/dfinterfaceDummy.h"
#include "eformat/SourceIdentifier.h"  
#include "eformat/write/ROBFragment.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/program_options.hpp>
#include <set>
#include <algorithm>
#include <sstream>
#include <cstdlib>
#include "dynlibs/DynamicLibrary.h"



/// Global object to emulate the service in charge of providing the available ROBs
std::set<uint32_t> robsInTheRun;  

/*! \breif Utility function that fill the set of robs available in the given run
 * Using a single detector for the sake of simplicity
 */
void fillRobsInTheRun(uint32_t size)
{
  for(uint32_t i=0; i < size; i++)
    robsInTheRun.insert( eformat::helper::SourceIdentifier(eformat::MUON_MDT_ENDCAP_A_SIDE, i).code() );
}


namespace po = boost::program_options;

int main(int argc, char** argv)
{
  // Command line parsing
  uint32_t     nEvts      = 3;
  uint32_t     hltRobSize = 4;
  uint32_t     nRobs      = 8;
  std::string  library    = "libdfinterfaceDummy";
  try 
  {
    po::options_description desc("Mockup program to test dfinterface");
    desc.add_options()
      ("nEvts,n"      , po::value<uint32_t>(&nEvts)      , "Number of events")
      ("hltRobSize,s" , po::value<uint32_t>(&hltRobSize) , "Size of the HLTROB")
      ("nRobs,r"      , po::value<uint32_t>(&nRobs)      , "Num of ROBs in the run")
      ("library,l"    , po::value<std::string>(&library) , "Name of the DLL to load") 
      ("help,h"                                          , "Print help message")
      ;
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);
    if( vm.count("help"))
      { std::cout << desc << std::endl; return 0; }
  }
  catch(...) { ERS_LOG("Caught boost::program_options exception"); }

  // Initialize the list of ROBs in the partition
  fillRobsInTheRun(nRobs);

  // Create empty property tree and set the parameters
  boost::property_tree::ptree pt;  
  pt.put("MaxNumEvents", nEvts);

  // Load the DLL
  typedef std::unique_ptr<daq::dfinterface::Session> (*builder_t)(const boost::property_tree::ptree& configuration);
  builder_t build;
  try { 
    daq::dynlibs::DynamicLibrary dynLib(library);
    build =  dynLib.function<builder_t>("createSession"); // 
  }
  catch(dynlibs::DynamicLibraryNotFound &ex) { ERS_LOG("DLL failure: "<< ex);        exit(EXIT_FAILURE);}
  catch(std::exception &ex)                  { ERS_LOG("Exception: "  << ex.what()); exit(EXIT_FAILURE);}   
  
  // Create the Session using the "createSession" function from the DLL
  std::unique_ptr<daq::dfinterface::Session> s;
  try { s = build(pt); }
  catch(daq::dfinterface::BadConfiguration &ex) { 
    ERS_LOG("Caught BadConfiguration: " << ex.what() ); 
    exit(EXIT_FAILURE);
  }

  // Open the session passing the application name and catch the error
  try { s->open("HLTPU-77"); }
  catch(daq::dfinterface::CommunicationError &ex) 
       { ERS_LOG("Communication error: " << ex.what()); exit(EXIT_FAILURE);}
  catch(std::exception &ex) { ERS_LOG("Exception: "  << ex.what()); exit(EXIT_FAILURE);}   

  //---- Event loop ------
  std::ostringstream o;
  for(uint32_t j=0; ; j++)
  {
    std::unique_ptr<daq::dfinterface::Event> e;     // The dfinterface::Event
    std::vector<eformat::read::ROBFragment>  rois;  // The ROBS in the RoI

    // 1. Request the next event (L1Result)
    {
      try { e=s->getNext(); }
      catch(daq::dfinterface::NoMoreEvents       &ex) { ERS_LOG("NoMoreEvents => break");                  break;  }
      catch(daq::dfinterface::CommunicationError &ex) { ERS_LOG("Communication error: " << ex); exit(EXIT_FAILURE);}
      catch(std::exception &ex)                       { ERS_LOG("Exception: "  << ex.what());   exit(EXIT_FAILURE);}   

      e->getL1Result(rois);
      for(auto &r: rois)
        { ERS_LOG("Event " << e->l1Id() << ", got L1Result: " << eformat::helper::SourceIdentifier(r.rob_source_id()).human() ); }
    }

    // 2. Data collection loop. Only one for the sake of simplicity
    {
      std::vector<hltinterface::DCM_ROBInfo> robInfos;
      for(int k=0; k < 1; k++)
      {
        // 2.1 Request a couple of ROBs
        std::vector<uint32_t> robIds; 
        std::copy_n(robsInTheRun.begin(), 2, std::back_inserter(robIds));
        try { e->getRobs(robIds, robInfos); }
        catch(daq::dfinterface::CommunicationError &ex) { ERS_LOG("Communication error: " << ex); exit(EXIT_FAILURE);}
        catch(std::exception &ex)                       { ERS_LOG("Exception: "  << ex.what());   exit(EXIT_FAILURE);}   

        // 2.2 Check the ROBIDs of the returned ROBs
        for(auto &r: robInfos) { 
          auto elapsed = r.robDeliveryTime - r.robRequestTime;
          o << std::hex << r.robFragment.source_id() << std::dec 
            << " (" << elapsed.count() <<  " tks), ";  
        }  
        ERS_LOG("L2 DC got ROBs: " << o.str()); 

        // 2.3 Possibly informs dfinterface about ROBs that may be requested in the future
        robIds.clear();
        std::copy_n(++robsInTheRun.rend(), 2, std::back_inserter(robIds));
        try { e->mayGetRobs(robIds); }
        catch(daq::dfinterface::CommunicationError &ex) { ERS_LOG("Communication error: " << ex); exit(EXIT_FAILURE);}
        catch(std::exception &ex)                       { ERS_LOG("Exception: "  << ex.what());   exit(EXIT_FAILURE);}   
      }
    }  
 
    // 3. Event building
    {
      std::vector<hltinterface::DCM_ROBInfo> allRobInfos;
      try { e->getAllRobs(allRobInfos); }
      catch(daq::dfinterface::CommunicationError &ex) { ERS_LOG("Communication error: " << ex); exit(EXIT_FAILURE);}
      catch(std::exception &ex)                       { ERS_LOG("Exception: "  << ex.what());   exit(EXIT_FAILURE);}   

      // 3.1 Check the ROBIDs of the returned ROBs
      o.str(std::string());
      for(auto &r: allRobInfos) {
          auto elapsed = r.robDeliveryTime - r.robRequestTime;
          o << std::hex << r.robFragment.source_id() << std::dec 
            << " (" << elapsed.count() <<  " tks), ";  
      }  
      ERS_LOG("EB DC got ROBs: " << o.str()); 
    }  
 
    // 4. Return decision to DCM
    //    E.g.: Accept one event every two
    {
      if(j%2==0) 
      {
        ERS_LOG("Event " << e->l1Id() << " rejected" ); 
        try { s->reject(std::move(e)); }
        catch(daq::dfinterface::CommunicationError &ex) { ERS_LOG("Communication error: " << ex); exit(EXIT_FAILURE);}
        catch(std::exception &ex)                       { ERS_LOG("Exception: "  << ex.what());   exit(EXIT_FAILURE);}   
      }  
      else
      {
        ERS_LOG("Event " << e->l1Id() << " accepted" ); 
        // 4.1 Create the HLTResult ROB
        uint32_t       source_id  = eformat::helper::SourceIdentifier(eformat::TDAQ_EVENT_FILTER, 1).code();
        uint32_t       *data      = new uint32_t[hltRobSize];
        for(uint32_t i=0; i< hltRobSize; i++) data[i] = 0xF00BA000+i;
        const uint32_t ndata = hltRobSize;
        eformat::write::ROBFragment hltrobw(source_id, source_id, rois[0].rod_run_no(), rois[0].rod_lvl1_id(), 
          rois[0].rod_bc_id(), rois[0].rod_lvl1_trigger_type(), rois[0].rod_detev_type(), ndata, data, 1);

        // 4.2 Serialize HLTResult in memory  
        const eformat::write::node_t * nodes = hltrobw.bind();     
        uint32_t* buf = new uint32_t[hltrobw.size_word()];
        eformat::write::copy(*nodes, buf, hltrobw.size_word());

        // 4.3 Create a eformat::read::ROBFragment to be passed to DCM
        eformat::read::ROBFragment hltrob(buf);

        // 4.4 Create Stream tag, triggerInfo, pscErrors
        eformat::helper::StreamTag t("Dummy", eformat::PHYSICS_TAG, true);
        std::vector<eformat::helper::StreamTag> st;
        st.push_back(t);
        std::vector<uint32_t> pscErrors;
        std::vector<uint32_t> triggerInfo;
        triggerInfo.push_back(0xFEA00000);
        triggerInfo.push_back(0xFEA00002);
       
        // 4.5 Use session::accept()
        try { s->accept(std::move(e), triggerInfo, st, pscErrors, hltrob); }
        catch(daq::dfinterface::CommunicationError &ex) { ERS_LOG("Communication error: " << ex); exit(EXIT_FAILURE);}
        catch(std::exception &ex)                       { ERS_LOG("Exception: "  << ex.what());   exit(EXIT_FAILURE);}   
      
        // 4.6 Delete the buffer
        delete[] buf;
        delete[] data;
      } // else (accept)
    } // Reject/accept stage 
  } // event loop 
  ERS_LOG("Exited from event loop"); 
 
  return 0;
}  
